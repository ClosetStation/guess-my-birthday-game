from random import randint

name = input("Hi! What is your name?")

# for loop through guess range [1,6]
for guess_number in range(1,6):
    month_number = randint(1,12)
    year_number = randint(1990,2000)

    print("Guess", guess_number, "were you born in",
    month_number, "/", year_number, "?")
# Reponse to birthday questions
    response = input("yes or no? ")

# Conditionals
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number ==5:
        print("I have other things to do. Good bye!")
    else:
        print("Drat! Lemme try again!")
# Catch all phrase
